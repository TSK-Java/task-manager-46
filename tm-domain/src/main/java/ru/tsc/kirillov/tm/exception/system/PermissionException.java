package ru.tsc.kirillov.tm.exception.system;

import ru.tsc.kirillov.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Ошибка! Недостаточно полномочий.");
    }

}
