package ru.tsc.kirillov.tm.exception.field;

public final class EmailAlreadyExistsException extends AbstractFieldException {

    public EmailAlreadyExistsException() {
        super("Ошибка! E-mail уже существует в системе.");
    }

    public EmailAlreadyExistsException(final String email) {
        super("Ошибка! E-mail `" + email + "` уже существует в системе.");
    }

}
